Useful anti-routine script for daily diary notes taking with plain folder/files structure. 
<!-- Creating, naming, saving notes in specific folder and it's opening in editor. -->
WSL is used. System variables and aliases for terminal commands are required in shell config.

![How it works](how-it-works.gif)